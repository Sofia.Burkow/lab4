package datastructure;

import cellular.CellState;

import java.util.ArrayList;

public class CellGrid implements IGrid {

    int rows;
    int columns;
    CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
        if (rows <= 0) {
            throw new IllegalArgumentException("The grid needs at least 1 row, you supplied " + rows);
        }
        this.rows = rows;
        if (columns <= 0) {
            throw new IllegalArgumentException("The grid needs at least 1 column, you supplied " + columns);
        }
        this.columns = columns;
        grid = new CellState[rows][columns];

        for ( int r = 0 ; r < rows ; r++ ) {
            for ( int c = 0 ; c < columns ; c++ ) {
                grid[r][c]= initialState;
            }
        }
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return columns;
    }


    @Override
    public void set(int row, int column, CellState element) {
        if (row < 0 || row >= rows) {
            throw new IndexOutOfBoundsException("Illegal row value.");
        }
        if (column < 0 || column >= columns) {
            throw new IndexOutOfBoundsException("Illegal column value.");
        }
        grid[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        if (row < 0 || row >= rows) {
            throw new IndexOutOfBoundsException("Illegal row value.");
        }
        if (column < 0 || column >= columns) {
            throw new IndexOutOfBoundsException("Illegal column value.");
        }
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid copyGrid = new CellGrid(rows, columns, null);
        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < columns; c++) {
                copyGrid.set(r, c, this.get(r, c));
            }
        }
        return copyGrid;
    }

}
